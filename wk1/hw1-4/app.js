var MongoClient = require('mongodb');


MongoClient.connect('mongodb://localhost:27017/accounts', function(err, db) {
    if(err) throw err;



    db.collection('accounts').findOne(function(err, doc) {
        if(err) throw err;

        if (!doc) {
            console.dir("No document found");
            return db.close();
        }


        console.log("Answer: " + doc.toString());
        return db.close();
    });
});
