use test;
db.grades.aggregate(
    { $unwind: "$scores" },
    { $match:
    { "scores.type": { $ne: "quiz" }}},
    { $group:
    { _id: { student_id: "$student_id", class_id: "$class_id" },
        avgScore: { $avg: "$scores.score" }}},
    { $group:
    { _id: "$_id.class_id",
        avgClassScore: { $avg: "$avgScore" }}},
    { $sort: { avgClassScore: -1 }},
  //{ $sort: { avgClassScore: 1 }},
    { $limit: 3 }
);

//
//2015-02-07T09:27:33.761-0600 imported 280 objects
//Davids-MacBook-Pro:grades davidtan$ cat app.js | mongo
//MongoDB shell version: 2.6.6
//connecting to: test
//switched to db test
//{ "_id" : 1, "avgClassScore" : 64.50642324269175 }
//{ "_id" : 5, "avgClassScore" : 58.08448767613548 }
//{ "_id" : 20, "avgClassScore" : 57.6309834548989 }
//bye
//Davids-MacBook-Pro:grades davidtan$


//Davids-MacBook-Pro:grades davidtan$ cat app.js | mongo
//MongoDB shell version: 2.6.6
//connecting to: test
//switched to db test
//{ "_id" : 2, "avgClassScore" : 37.61742117387635 }
//{ "_id" : 12, "avgClassScore" : 40.62345969481146 }
//{ "_id" : 8, "avgClassScore" : 41.303883810000194 }
//bye
//Davids-MacBook-Pro:grades davidtan$
