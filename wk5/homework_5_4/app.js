use test;
db.zips.aggregate(
    { $project:
    { first_char: { $substr: ["$city",0,1]}, pop: "$pop"}
    },
    { $match:
    { first_char: { $lte: "9" }}
    },
    { $group:
    { _id: "rural", rpop: { $sum: "$pop" }}
    }
);
//
//Davids-MacBook-Pro:homework_5_4 davidtan$ ls
//app.js                          notes
//homework_5_4_Instructions       zips.json
//Davids-MacBook-Pro:homework_5_4 davidtan$ cat app.js | mongo
//MongoDB shell version: 2.6.6
//connecting to: test
//switched to db test
//{ "_id" : "rural", "rpop" : 298015 }
//bye
//Davids-MacBook-Pro:homework_5_4 davidtan$
