use
enron;
db.messages.aggregate([
    /*Part 1 For this question you will use the aggregation framework to figure out pairs of people that tend to communicate a lot.
     To do this, you will need to unwind the To list for each message. */
    {$unwind: '$headers.To'},
    /* create the From and To  array */
    {$group: {_id: '$_id', From: {$first: '$headers.From'}, To: {$addToSet: '$headers.To'}}},
    /*Part 2 This problem is a little tricky because a recipient may appear more than once in the
     To list for a message. */
    /* unwind by To*/
    {$unwind: '$To'},
    /* now group by From and To, counting each them */
    {$group: {_id: {From: '$From', To: '$To'}, total: {$sum: 1}}},
    /* sort by popularity */
    {$sort: {total: -1}},
    {$limit: 1},

    /* reshape for beauty */
    {$project: {_id: 0, From: '$_id.From', To: '$_id.To', Total: '$total'}}
])


//Davids-MacBook-Pro:question_2 davidtan$ cat final_q2_part3.js | mongo
//MongoDB shell version: 2.6.6
//connecting to: test
//switched to db enron
//{ "_id" : { "From" : "susan.mara@enron.com", "To" : "jeff.dasovich@enron.com" }, "total" : 750 }
//bye
//Davids-MacBook-Pro:question_2 davidtan$ cat final_q2_part3.js | mongo
//MongoDB shell version: 2.6.6
//connecting to: test
//switched to db enron
//{ "From" : "susan.mara@enron.com", "To" : "jeff.dasovich@enron.com", "Total" : 750 }
//bye
//Davids-MacBook-Pro:question_2 davidtan$

