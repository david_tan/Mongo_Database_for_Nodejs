use enron;
db.messages.aggregate([
    //Part 1
    {$unwind: '$headers.To'},
    /* create the From and To  array */
    {$group: {_id : '$_id', From : {$first: '$headers.From'}, To: {$addToSet : '$headers.To'}}},
    //Part 2
    //This problem is a little tricky because a recipient may appear more than once in the
    // To list for a message.
    /* unwind by To*/
    {$unwind: '$To'},
    /* now group by From and To, counting each them */
    {$group: {_id: { From:'$From', To:'$To' }, total : {$sum : 1}}},
    /* sort by popularity */
    {$sort: { total: -1}}
])

Davids-MacBook-Pro:question_2 davidtan$ cat final_q2_part2.js | mongo
MongoDB shell version: 2.6.6
connecting to: test
switched to db enron
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "jeff.dasovich@enron.com" }, "total" : 750 }
{ "_id" : { "From" : "soblander@carrfut.com", "To" : "soblander@carrfut.com" }, "total" : 679 }
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "james.steffes@enron.com" }, "total" : 646 }
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "richard.shapiro@enron.com" }, "total" : 616 }
{ "_id" : { "From" : "evelyn.metoyer@enron.com", "To" : "kate.symes@enron.com" }, "total" : 567 }
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "karen.denne@enron.com" }, "total" : 552 }
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "alan.comnes@enron.com" }, "total" : 550 }
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "paul.kaufman@enron.com" }, "total" : 506 }
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "harry.kingerski@enron.com" }, "total" : 489 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "paul.kaufman@enron.com" }, "total" : 488 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "jdasovic@enron.com" }, "total" : 485 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "harry.kingerski@enron.com" }, "total" : 481 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "smara@enron.com" }, "total" : 467 }
{ "_id" : { "From" : "susan.mara@enron.com", "To" : "sandra.mccubbin@enron.com" }, "total" : 464 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "mday@gmssr.com" }, "total" : 464 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "steven.j.kean@enron.com" }, "total" : 460 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "rshapiro@enron.com" }, "total" : 457 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "bhansen@lhom.com" }, "total" : 457 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "james.d.steffes@enron.com" }, "total" : 454 }
{ "_id" : { "From" : "sgovenar@govadv.com", "To" : "hgovenar@govadv.com" }, "total" : 452 }
Type "it" for more
    bye
