var express = require('express')
    , app = express() // Web framework to handle routing requests
    , cons = require('consolidate') // Templating library adapter for Express
    , MongoClient = require('mongodb').MongoClient // Driver for connecting to MongoDB
    , routes = require('./routes'); // Routes for our application ##include local file

//http://stackoverflow.com/questions/24012708/connect-multipart-use-parser-multiparty-busboy-formidable
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//var uri = 'mongodb://localhost:27017/blog';
var uri2 = 'mongodb://heroku_app1234:password@ds029821.mongolab.com:29821/heroku_app1234';

MongoClient.connect(uri2, function (err, db) {
    "use strict";
    if (err) throw err;

    // Register our templating engine
    app.engine('html', cons.swig);
    app.set('view engine', 'html');
    app.set('views', __dirname + '/views');

    app.use(cookieParser());

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());

    /* ORIGINAL*/
    //// Express middleware to populate 'req.cookies' so we can access cookies
    //app.use(express.cookieParser());
    //
    //// Express middleware to populate 'req.body' so we can access POST variables
    //app.use(express.bodyParser());


    // Application routes
    routes(app, db);

    //Goes to heroku
    app.listen(process.env.PORT || 3000);
    //app.listen(3000);
    console.log('Express server listening on process.env.PORT or port 3000');
});
