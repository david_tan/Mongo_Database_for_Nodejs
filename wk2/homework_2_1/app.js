var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/weather', function(err, db) {
    if(err) throw err;

    var query = { 'Wind Direction' : { "$gt" : 180, "$lt" : 360 } };

    var projection = { 'State' : 1, '_id' : 0 };

    db.collection('data').find(query, projection).sort({ "Temperature" : 1}).limit(1).toArray(function(err, docs) {
        if(err) throw err;

        docs.forEach(function (doc) {
            console.dir(doc);
            console.dir(doc.State + " Is the first one!");
        });

        db.close();
    });
});
/*

*****************
*
* ANSWER HOMEWORK 2-1 New Mexico
* **************
 {
 "_id" : ObjectId("54ba94fd2f63eff0044845d2"),
 "Day" : 24,
 "Time" : 153,
 "State" : "New Mexico",
 "Airport" : "SAF",
 "Temperature" : 0,
 "Humidity" : 87,
 "Wind Speed" : 5,
 "Wind Direction" : 260,
 "Station Pressure" : 23.88,
 "Sea Level Pressure" : 274
 }
 */