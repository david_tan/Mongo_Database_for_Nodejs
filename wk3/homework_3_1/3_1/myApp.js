var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/school', function (err, db) {
    if (err) throw err;

    var cursor = db.collection('students').find({});

    cursor.each(function (err, doc) {
        if (err) throw err;

        if (doc == null) {
            return db.close();
        }
        var min = 101.0000;
        var ctr = 0;//reset
        var minL = 0; //minimum location at scores

        doc.scores.filter(function (x, i) {
            if (x.type === 'homework') {
                if(ctr === 0){min= x.score;}
                if (x.score < min) {
                    min = x.score;
                    minL = i;
                }
                ctr++;
            }
        });

        //Update new document
        if (ctr > 1) { // splice only when > 1
            doc.scores.splice(minL, 1);
            db.collection('students').save(doc, function (err, saved) {
                if (err) throw err;
                console.log("Number of document saved successfully : " + saved);

            });
        }


    }); //each document

});